// console.log("Hello World!");

/*
Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
*/

let getCube = 2 ** 3;

/*
Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is… */

console.log(`The cube of 2 is ${getCube}`);
/*
Create a variable address with a value of an array containing details of an address.
*/
let address = ['San Guillermo St', 'La Verna Hills Subdivision', 'Davao City', 'Davao del Sur'];
/*
Destructure the array and print out a message with the full address using Template Literals.
*/
let [street, subdivision, city, province] = address;

console.log(`I live at ${subdivision}, ${street}, ${city}, ${province}`);
/*
Create a variable animal with a value of an object data type with different animal details as it’s properties.
*/
let animal = {
	name: 'Woody',
	breed: 'pug',
	color: 'fawn',
	size: 'small'
};
/*
Destructure the object and print out a message with the details of the animal using Template Literals.
*/
let {name, breed, color, size} = animal

console.log(`${name} is a ${breed} his color is ${color} and his size is ${size}.`);
/*
Create an array of numbers.
*/
let array = [1, 2, 3, 4, 5];
/*
Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/
array.forEach(num => console.log(num));
/*
Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/
const reduceNumber = array.reduce((x , y) => {
	return x + y;
});

console.log(reduceNumber);

/*
Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
*/
class Dog {

	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}
/*
Create/instantiate a new object from the class Dog and console log the object.
(3 dog objects)
*/

let dog1 = new Dog ("Woody", "4", "Pug");
let dog2 = new Dog ("Bella", "6", "Labrador Retriever");
let dog3 = new Dog ("Max", "7", "French Bulldog");

console.log(dog1);
console.log(dog2);
console.log(dog3);